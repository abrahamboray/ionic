import { Component } from '@angular/core';
import { Http } from "@angular/http";
import "rxjs/add/operator/map";

import { NavController } from 'ionic-angular';
import { ProductService } from "../../providers/product-service";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(private productService: ProductService, private http: Http, public navCtrl: NavController) {

  }

  ionViewDidLoad(){
    this.productService.getProducts()
      .subscribe(respone => console.log(respone));    
  }

}
